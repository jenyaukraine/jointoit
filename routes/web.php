<?php
Auth::routes(['register' => false]);
Route::get('/language/{locale}', function($locale){
    session()->put('locale', $locale);
    return redirect()->back();
});
Route::resource('/', 'CompaniesController')->middleware('auth');

// Dont working.
Route::get('/{id}/edit', 'CompaniesController@edit')->middleware('auth');
Route::get('/{id}', 'CompaniesController@destroy')->middleware('auth');
// ***********

Route::resource('{company_id}/employees', 'EmployeesController')->middleware('auth');
Route::get('{company_id}/employees/{id}/edit', 'EmployeesController@edit')->middleware('auth');
Route::delete('{company_id}/employees/{id}', 'EmployeesController@destroy')->middleware('auth');
Route::get('{company_id}/employees/create', 'EmployeesController@create')->middleware('auth');