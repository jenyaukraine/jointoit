$(document).ready( function () {
    var table = $('#myTable').DataTable({
        "language": {
            "paginate": {
                first:    '«',
                previous: '‹',
                next:     '›',
                last:     '»'
            }
        },
        "lengthChange": false,
        "searching": false,
        "info": false
    });

    $('#myTable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
} );
function create(){
    location.href = "/create"; 
}

function destroy(t){
    t = parseInt(t);
    if(isNaN(t))
        return;
    $.ajax({
        url: '/'+t,
        type: 'GET',
        success: function(result) {
            $('#myTable tr.selected').remove();
        }
    });
}
function edit(t){
    t = parseInt(t);
    if(isNaN(t))
        return;
        location.href = "/"+t+"/edit"; 
}

/* employees?! */
function open_employees(t){
    t = parseInt(t);
    if(isNaN(t))
        return;
    location.href = t+"/employees"; 
}

function create_employees(){
    location.href = "employees/create"; 
}

function destroy_employees2(t){
    t = parseInt(t);
    if(isNaN(t))
        return;
    $.ajax({
        url: 'employees/'+t,
        type: 'DELETE',
        success: function(result) {
            $('#myTable tr.selected').remove();
        }
    });
}
function destroy_employees(t){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax(
    {
        url: 'employees/' + t,
        type: 'DELETE', // replaced from put
        dataType: "JSON",
        success: function (response)
        {
            console.log(response); // see the reponse sent
        },
        error: function(xhr) {
         console.log(xhr.responseText); // this line will save you tons of hours while debugging
        // do something here because of error
       }
    });
}
function edit_employees(t){
    t = parseInt(t);
    if(isNaN(t))
        return;
        location.href = "employees/"+t+"/edit"; 
}