<?php

return [
    'Login' => 'Login',
    'Logout' => 'Logout',
    'Create' => 'Create',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Logo' => 'Logo',
    'Name' => 'Name',
    'Email' => 'Email',
    'Website' => 'Website',
    'CompanyName' => 'Company Name',
    'Company__placeholder' => 'What is yours complany name?',
    'Email__placeholder' => 'Write your email address',
    'Website__placeholder' => 'Show me your site',
    'Save' => 'Save',
    'Update' => 'Update',
    'Change__Language' => 'Change Language',
    'English' => 'English',
    'Russian' => 'Russian',
    'Password' => 'Password',
    'Remember__Me' => 'Remember Me',
    'Forgot__Your__Password' => 'Forgot Your Password',
    'Reset__Password' => 'Reset Password',
    'First__name' => 'First name',
    'Last__name' => 'Last name',
    'Telephone' => 'Telephone',
    'First__name__placeholder' => 'What is your name?',
    'Last__name__placeholder' => 'What is your last name?',
    'Telephone__placeholder' => 'Left your mobile phone number',
    'Employees' => 'Employees'
    
];