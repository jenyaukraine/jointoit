<?php

return [
    'Login' => 'Войти',
    'Logout' => 'Выход',
    'Create' => 'Создать',
    'Edit' => 'Редактирование',
    'Delete' => 'Удалить',
    'Logo' => 'Логотип',
    'Name' => 'Имя',
    'Email' => 'Почта',
    'Website' => 'Сайт',
    'CompanyName' => 'Название компании',
    'Company__placeholder' => 'Как называется ваша компания?',
    'Email__placeholder' => 'Введите ваш почтовый адрес',
    'Website__placeholder' => 'Покажите нам ваш сайт',
    'Save' => 'Сохранить',
    'Update' => 'Обновить',
    'Change__Language' => 'Изменить язык',
    'English' => 'Английский',
    'Russian' => 'Русский',
    'Password' => 'Пароль',
    'Remember__Me' => 'Запомнить меня',
    'Forgot__Your__Password' => 'Забыли ваш пароль?',
    'Reset__Password' => 'Сбросить пароль',
    'First__name' => 'Имя',
    'Last__name' => 'Фамилия',
    'Telephone' => 'Телефон',
    'First__name__placeholder' => 'Как вас зовут?',
    'Last__name__placeholder' => 'Напишите вашу фамилию',
    'Telephone__placeholder' => 'Оставьте ваш телефон',
    'Employees' => 'Сотрудники'
 
];