@extends('layouts.app')

@section('content')
<style>
tbody tr.selected {
    background-color: #aab7d1;
}
.padding_buttons button{
    margin:10px;
}
</style>
                        <div class="row justify-content-md-center">
                            <div class=".col-md-6 .col-md-offset-3 padding_buttons ">
                            <button class='btn btn-info' onclick="open_employees($('.selected').attr('id'))">{{ __('messages.Employees') }}</button>

                                <button class='btn btn-success' onclick="create()">{{ __('messages.Create') }}</button>
                                <button class='btn btn-warning' onclick="edit($('.selected').attr('id'))">{{ __('messages.Edit') }}</button>
                                <button class='btn btn-danger' onclick="destroy($('.selected').attr('id'))">{{ __('messages.Delete') }}</button>
                            </div>
                        </div>
                        
                        <table id="myTable" class="table table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 51px;">{{ __('messages.Logo') }}</th>
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 65px;">{{ __('messages.Email') }}</th>
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 45px;">{{ __('messages.Name') }}</th>
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 26px;">{{ __('messages.Website') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $item)						
                            <tr role="row" id="{{$item->id}}">
                            <td>
                             @if($item->logo)
                             <img height=100px src="{{ asset('storage/'.$item->logo) }}"> 
                             @endif
                         </td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->website}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">{{ __('messages.Logo') }}</th>
                                <th rowspan="1" colspan="1">{{ __('messages.Email') }}</th>
                                <th rowspan="1" colspan="1">{{ __('messages.Name') }}</th>
                                <th rowspan="1" colspan="1">{{ __('messages.Website') }}</th>
                            </tr>
                        </tfoot>
                        </table>
@endsection
