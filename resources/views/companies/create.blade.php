@extends('layouts.app')
@section('content')
<div class="row">
   <div class="col-md-12 col-md-offset-3">
      {{ Form::open(['route'=>'store', 'files' => 'true','enctype'=>'multipart/form-data']) }}
      <div class="row">
         <div class="col-sm-4">
            {!! form::label('Company__Name',trans('messages.CompanyName')) !!}
         </div>
         <div class="col-sm-8">
            <div class="form-group {{ $errors->has('title') ? 'has-error' : "" }}">
            @if(isset($model))
               {{ Form::hidden('id', $model->id) }}
            @endif
            {{ Form::text('name',NULL, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>trans('messages.Company__placeholder')]) }}
            <p class="help-block">{{ $errors->first('title', ':message') }}</p>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         {!! form::label('Company__Email',trans('messages.Email')) !!}
      </div>
      <div class="col-sm-8">
         <div class="form-group {{ $errors->has('body') ? 'has-error' : "" }}">
         {{ Form::email('email',NULL, ['class'=>'form-control', 'id'=>'email', 'placeholder'=>trans('messages.Email__placeholder')]) }}
         <p class="help-block">{{ $errors->first('email', ':message') }}</p>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-4">
      {!! form::label('Company__Logo',trans('messages.Logo')) !!}
   </div>
   <div class="col-sm-8">
      <div class="form-group {{ $errors->has('body') ? 'has-error' : "" }}">
      {{ Form::file('logo',NULL, ['class'=>'form-control', 'id'=>'logo']) }}
      <p class="help-block">{{ $errors->first('logo', ':message') }}</p>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-4">
      {!! form::label('Company__Website',trans('messages.Website')) !!}
   </div>
   <div class="col-sm-8">
      <div class="form-group {{ $errors->has('body') ? 'has-error' : "" }}">
      {{ Form::text('website',NULL, ['class'=>'form-control', 'id'=>'website', 'placeholder'=>trans('messages.Website__placeholder')]) }}
      <p class="help-block">{{ $errors->first('website', ':message') }}</p>
   </div>
</div>
</div>
<div class="row justify-content-md-center">
   <div class="form-group">
      {{ Form::button(isset($model)? trans('messages.Update') : trans('messages.Save') , ['class'=>'btn btn-success', 'type'=>'submit']) }}
   </div>
</div>
{{ form::close() }}
</div>
</div>
@endsection