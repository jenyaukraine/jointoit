@extends('layouts.app')

@section('content')
<style>
tbody tr.selected {
    background-color: #aab7d1;
}
.padding_buttons button{
    margin:10px;
}
</style>
                        <div class="row justify-content-md-center">
                            <div class=".col-md-6 .col-md-offset-3 padding_buttons ">
                                <button class='btn btn-success' onclick="create_employees()">{{ __('messages.Create') }}</button>
                                <button class='btn btn-warning' onclick="edit_employees($('.selected').attr('id'))">{{ __('messages.Edit') }}</button>
                                <button class='btn btn-danger' onclick="destroy_employees($('.selected').attr('id'))">{{ __('messages.Delete') }}</button>
                            </div>
                        </div>

                        <div class="row justify-content-md-center">
                        @if(isset($company_name))
                            <h2>{{$company_name}}</h2>
                        @endif
                        </div>
                        
                        <table id="myTable" class="table table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 51px;">{{ __('messages.First__name') }}</th>
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 65px;">{{ __('messages.Last__name') }}</th>
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 45px;">{{ __('messages.Email') }}</th>
                                <th tabindex="0" rowspan="1" colspan="1" style="width: 26px;">{{ __('messages.Telephone') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $item)						
                            <tr role="row" id="{{$item->id}}">
                                <td>{{$item->first_name}}</td>
                                <td>{{$item->last_name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->phone}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">{{ __('messages.First__name') }}</th>
                                <th rowspan="1" colspan="1">{{ __('messages.Last__name') }}</th>
                                <th rowspan="1" colspan="1">{{ __('messages.Email') }}</th>
                                <th rowspan="1" colspan="1">{{ __('messages.Telephone') }}</th>
                            </tr>
                        </tfoot>
                        </table>
@endsection
