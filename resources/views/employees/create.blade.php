@extends('layouts.app')
@section('content')
<div class="row">
   <div class="col-md-12 col-md-offset-3">
      {{ Form::open(['route'=>array('employees.store', $company_id), 'files' => 'true','enctype'=>'multipart/form-data']) }}
      <div class="row">
         <div class="col-sm-4">
            {!! form::label('Company__Name',trans('messages.First__name')) !!}
         </div>
         <div class="col-sm-8">
            <div class="form-group {{ $errors->has('title') ? 'has-error' : "" }}">
            @if(isset($model))
               {{ Form::hidden('id', $model->id) }}
            @endif
            @if(isset($company_id))
               {{ Form::hidden('company_id', $company_id) }}
            @endif
            {{ Form::text('first_name',NULL, ['class'=>'form-control', 'id'=>'first_name', 'placeholder'=>trans('messages.First__name__placeholder')]) }}
            <p class="help-block">{{ $errors->first('title', ':message') }}</p>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         {!! form::label('Last__name',trans('messages.Last__name')) !!}
      </div>
      <div class="col-sm-8">
         <div class="form-group {{ $errors->has('body') ? 'has-error' : "" }}">
         {{ Form::text('last_name',NULL, ['class'=>'form-control', 'id'=>'last_name', 'placeholder'=>trans('messages.Last__name__placeholder')]) }}
         <p class="help-block">{{ $errors->first('last_name', ':message') }}</p>
      </div>
   </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         {!! form::label('Company__Email',trans('messages.Email')) !!}
      </div>
      <div class="col-sm-8">
         <div class="form-group {{ $errors->has('body') ? 'has-error' : "" }}">
         {{ Form::email('email',NULL, ['class'=>'form-control', 'id'=>'email', 'placeholder'=>trans('messages.Email__placeholder')]) }}
         <p class="help-block">{{ $errors->first('email', ':message') }}</p>
      </div>
   </div>

</div>
<div class="row">
   <div class="col-sm-4">
      {!! form::label('Company__Website',trans('messages.Telephone')) !!}
   </div>
   <div class="col-sm-8">
      <div class="form-group {{ $errors->has('body') ? 'has-error' : "" }}">
      {{ Form::text('phone',NULL, ['class'=>'form-control', 'id'=>'phone', 'placeholder'=>trans('messages.Telephone__placeholder')]) }}
      <p class="help-block">{{ $errors->first('phone', ':message') }}</p>
   </div>
</div>
</div>
<div class="row justify-content-md-center">
   <div class="form-group">
      {{ Form::button(isset($model)? trans('messages.Update') : trans('messages.Save') , ['class'=>'btn btn-success', 'type'=>'submit']) }}
   </div>
</div>
{{ form::close() }}
</div>
</div>
@endsection