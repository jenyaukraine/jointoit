<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCompanies;
use App\Companies as Companies;

class CompaniesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Model get
        $companies_data = \App\Companies::get();
        // Render blade
        return view("companies.index", ['companies' => $companies_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Render blade
        return view("companies.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanies $request)
    {
        // Validate
        $validated = $request->validated();
        
        $data = array();
            
        $data['name']= $validated['name'];
        $data['email'] = $validated['email'];

        // Image
        if(isset($validated['logo']))
        {
                $logo = $validated['logo']->getClientOriginalName();
                $data['logo'] = $validated['logo']->store('logos', ['disk' => 'public']);
        }
        $data['website'] = $validated['website'];
        
        if(is_numeric($request->get('id'))){
            Companies::find($request->get('id'))->update($data);         
        } else {
            Companies::updateOrCreate($data);
        }

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = \App\Companies::findOrFail($id);
        return view("companies.edit", ['model'=> $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCompanies $request,$id)
    {
          // Validate
          $validated = $request->validated();
        
          $date = array();
          $data['name'] = $validated['name'];
          $data['email'] = $validated['email'];
          // Image
          if(isset($validated['logo']))
          {
                  $logo = $validated['logo']->getClientOriginalName();
                  $data['logo'] = $validated['logo']->store('logos', ['disk' => 'public']);
          }
          $data['website'] = $validated['website'];
         
          $data = \App\Companies::updateOrCreate($data);
  
          return redirect('/');
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = \App\Companies::findOrFail($id);
        \Storage::disk('public')->delete($delete->logo);
        $delete->delete();
        return ['success' => true, 'message' => 'Operation ok.']; 
    }
}
