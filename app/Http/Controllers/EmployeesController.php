<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreEmployees;
use App\Employees as Employees;
use App\Companies as Companies;

class EmployeesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company_id)
    {
        // Model get
        $employees_data = Employees::where('company', $company_id)->get();
        $company_name = Companies::find($company_id)->name;
        // Render blade
        return view("employees.index", ['employees' => $employees_data, 'company_id'=>$company_id, 'company_name'=>$company_name]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($company_id)
    {
        // Render blade
        return view("employees.create", ['company_id' => $company_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployees $request,$company_id)
    {
        // Validate
        $validated = $request->validated();
        
        $data = array();
            
        $data['first_name']= $validated['first_name'];
        $data['last_name'] = $validated['last_name'];
        $data['email'] = $validated['email'];
        $data['phone'] = $validated['phone'];
        $data['company'] = $company_id;

        if(is_numeric($request->get('id'))){
            Employees::find($request->get('id'))->update($data);         
        } else {
            Employees::updateOrCreate($data);
        }

        return redirect($company_id.'/employees');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($company_id,$id)
    {
        $model = Employees::findOrFail($id);
        return view("employees.edit", ['model'=> $model, 'company_id'=>$company_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($company_id,$id)
    {
        $delete = Employees::findOrFail($id);
        $delete->delete();
        return ['success' => true, 'message' => 'Operation ok.']; 
    }
}
